package classes;
public class Apartamento {
    private int pavimentos;
    private int comodos;
    private float areaConstruida;
    private String localizacao;
    private boolean disponibilidadeDeVender;
    private boolean disponibilidadeDeAlugar;
    private float precoVenda;
    private float precoAluguel;


    public Apartamento(int pavimentos, int comodos, float areaConstruida, String localizacao, boolean disponibilidadeDeVender, boolean disponibilidadeDeAlugar, float precoVenda, float precoAluguel) {
        this.setPavimentos(pavimentos);
        this.setComodos(comodos);
        this.setAreaConstruida(areaConstruida);
        this.setLocalizacao(localizacao);
        this.setDisponibilidadeDeVender(disponibilidadeDeVender);
        this.setDisponibilidadeDeAlugar(disponibilidadeDeAlugar);
        this.setPrecoVenda(precoVenda);
        this.setPrecoAluguel(precoAluguel);
        this.vender(disponibilidadeDeVender, precoVenda);
        this.alugar(disponibilidadeDeAlugar, precoAluguel);
        this.reformar(pavimentos, comodos, areaConstruida);

    }


    public void setPavimentos(int pavimentos) {
        this.pavimentos = pavimentos;
    }
    public int getPavimentos() {
        return this.pavimentos;
    }
    public void setComodos(int comodos) {
        this.comodos = comodos;
    }
    public int getComodos() {
        return this.comodos;
    }
    public void setAreaConstruida(float areaConstruida) {
        this.areaConstruida = areaConstruida;
    }
    public float getAreaConstruida() {
        return this.areaConstruida;
    }
    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }
    public String getLocalizacao() {
        return this.localizacao;
    }
    public void setDisponibilidadeDeVender(boolean disponibilidadeDeVender) {
        this.disponibilidadeDeVender = disponibilidadeDeVender;
    }
    public boolean getDisponibilidadeDeVender() {
        return this.disponibilidadeDeVender;
    }
    public void setDisponibilidadeDeAlugar(boolean disponibilidadeDeAlugar) {
        this.disponibilidadeDeAlugar = disponibilidadeDeAlugar;
    }
    public boolean getDisponibilidadeDeAlugar() {
        return this.disponibilidadeDeAlugar;
    }
    public void setPrecoVenda(float precoVenda) {
        this.precoVenda = precoVenda;
    }
    public float getPrecoVenda() {
        return this.precoVenda;
    }
    public void setPrecoAluguel(float precoAluguel) {
        this.precoAluguel = precoAluguel;
    }
    public float getPrecoAluguel() {
        return this.precoAluguel;
    }


    
    public void vender(boolean disponibilidadeDeVender, float precoVenda) {
        this.disponibilidadeDeVender = disponibilidadeDeVender;
        this.precoVenda = precoVenda;
    }
       
    public void alugar(boolean disponibilidadeDeAlugar, float precoAluguel) {
        this.disponibilidadeDeAlugar = disponibilidadeDeAlugar;
        this.precoAluguel = precoAluguel;
    }

    public void reformar(int pavimentos, int comodos, float areaConstruida) {
        this.pavimentos = pavimentos;
        this.comodos = comodos;
        this.areaConstruida = areaConstruida;
    }
}